from wtforms import Form, StringField, TextAreaField, SubmitField, validators, ValidationError, PasswordField


class LogInForm(Form):
    login = StringField("Login", [validators.Length(min=4, max=35)])
    password = PasswordField("Password", [validators.DataRequired(), validators.Length(min=4, max=100)])
    submit = SubmitField("Log In")


class ContactForm(Form):
    subject = StringField("Sujet", [validators.Length(min=4, max=35)])
    name = StringField("Nom", [validators.Length(min=4, max=35)])
    email = StringField("Email", [validators.Length(min=4, max=35), validators.Email()])
    message = TextAreaField("Message", [validators.Length(min=4, max=500)])
    submit = SubmitField("Submit")
