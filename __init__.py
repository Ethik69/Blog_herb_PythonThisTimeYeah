#! /usr/bin/python
# -*- coding:utf-8 -*-
from flask import Flask, redirect, render_template, session, request, flash, g
from flask_mail import Mail, Message
from form import ContactForm, LogInForm
import constant
import rethinkdb as rdb
from rethinkdb.errors import RqlRuntimeError, RqlDriverError
import hashlib

app = Flask(__name__, static_url_path='')

mail_settings = {'MAIL_SERVER': constant.MAIL_SERVER,
                 'MAIL_PORT': constant.MAIL_PORT,
                 'MAIL_USE_TLS': constant.MAIL_USE_TLS,
                 'MAIL_USE_SSL': constant.MAIL_USE_SSL,
                 'MAIL_USERNAME': constant.MAIL_USERNAME,
                 'MAIL_PASSWORD': constant.MAIL_PASSWORD}

app.config.update(mail_settings)
mail = Mail(app)

app.config.update(dict(DEBUG=constant.DEBUG,
                       RDB_HOST=constant.RDB_HOST,
                       RDB_PORT=constant.RDB_PORT,
                       DB_NAME=constant.DB_NAME))

# ----------------Gestion DB------------------


@app.before_request
def before_request():
    try:
        # g.rdb_conn = rdb.connect(host=RDB_HOST, port=RDB_PORT, db=DB_NAME, auth_key=DB_KEY)
        g.rdb_conn = rdb.connect(host=app.config['RDB_HOST'],
                                 port=app.config['RDB_PORT'],
                                 db=app.config['DB_NAME'])
    except RqlDriverError:
        abort(503, "No database connection could be established.")


def insert(table, data_dict):
    """ data_dict = {'name': 'test', 'email': 'test@test.fr'} """
    rdb.db(app.config['DB_NAME']).table(table).insert([data_dict]).run(g.rdb_conn)


def select_all(table):
    return rdb.db(app.config['DB_NAME']).table(table).run(g.rdb_conn)


def select(table, id):
    return rdb.db(app.config['DB_NAME']).table(table).get(id).run(g.rdb_conn)


def filter(table, patern):
    return rdb.db(app.config['DB_NAME']).table(table).filter(patern).count().run(g.rdb_conn)


def delete(table, id):
    rdb.db(app.config['DB_NAME']).table(table).get(id).delete().run(g.rdb_conn)


@app.teardown_request
def teardown_request(exception):
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass


# ----------------Gestion Url-----------------


@app.route('/', methods=['GET'])
def index():
    return render_template('body_index.html', title="Accueil")


@app.route('/about/', methods=['GET'])
def about():
    return render_template('body_about.html', title="About")


@app.route('/article/', methods=['GET'])
def article():
    return render_template('body_article.html', title="Article")


@app.route('/back/', methods=['GET', 'POST'])
def back():
    return render_template('body_back.html', title="Back")


@app.route('/log/', methods=['GET', 'POST'])
def login():
    form = LogInForm(request.form)
    if request.method == 'POST':
        if not form.validate():
            flash('Try Again.')
        else:
            login = form.login.data
            password = form.password.data
            password += constant.SALT
            md5 = hashlib.md5(password.encode()).hexdigest()
            result = filter('admin', {'username': login, 'password': md5})
            if result == 1:
                flash('Yeah !')
            #TODO: finish login =)

    return render_template('body_login.html', title="Login", form=form)


@app.route('/contact/', methods=['GET', 'POST'])
def contact():
    form = ContactForm(request.form)
    if request.method == 'POST':
        if not form.validate():
            flash('Veuillez remplir correctement tout les champs.')
        else:
            name = form.name.data
            email = form.email.data
            subject = form.subject.data
            message_body = 'De: {} Email: {} Message {}'.format(name, email, form.message.data)

            msg = Message(subject='Herb Contact: ' + subject,
                          sender=app.config.get("MAIL_USERNAME"),
                          recipients=constant.CONTACT_MAIL.split(),
                          body=message_body)
            mail.send(msg)
            flash('Formulaire bien envoyé !')

    return render_template('body_contact.html', title="Contact", form=form)


@app.route('/herbarium/', methods=['GET', 'POST'])
def herbarium():
    return render_template('body_herbarium.html', title="Herbarium")


@app.route('/preparation/', methods=['GET', 'POST'])
def preparation():
    return render_template('body_preparation.html', title="Preparation")


if __name__ == '__main__':
    app.secret_key = constant.SECRET_KEY
    app.config['SESSION_TYPE'] = 'filesystem'
    app.run()



#TODO: finish login
# Create requirements.txt
# pipreqs Blog_herb_PythonThisTimeYeah/
